'use strict';

//if = sentencia de control

var edad = 19;

if(edad >= 18){
    console.log('Eres mayor de edad');
}else{
    console.log('No es mayor de edad');
}

// == comparador exacto
// > Mayor que
// < Menor que
// != diferente a

// && = AND ambas verdaderas igual a true
// || = OR una verdadera igual a true
// ! = NOT si una es falsa da false
