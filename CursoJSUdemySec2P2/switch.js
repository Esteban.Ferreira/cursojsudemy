'use strict';

var edad = 20;
var texto = '';

// switch -> "En caso que"
// 

switch(edad){
    case 17:
    texto = 'No tenes 18';    
    break;    
    case 20:
    texto = 'Mayor de edad';
    break;
    // break -> saltamos al siguiente caso. De otra manera no cortaría y seguiría evaluando el resto 
    //de los casos lo que ralentiza nuestro programa
    
    // default -> si no se cumple ninguna condición definimos el valor de la variable de salida 
    //(en este caso texto)
    default:
        texto = 'No se definió edad';
}

console.log(texto);