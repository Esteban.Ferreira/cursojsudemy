'use strict';

// 'use strict' obliga a buenas prácticas que propone IPV6

// let modifica una variable dentro del bloque que la ejecuta (en este caso el if). Fuera de este tiene su valor inicial.

var numero = 20;

if(true){
    let numero = 40;

    console.log(numero);
        
}

console.log(numero);
    